﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //configs
    [SerializeField] float projectileSpeed = 5f;
    private float gunDamagePoint = 5f;
    private int direction = 1;

    //caches
    Rigidbody2D rigidbody2D;

    //Setters Getters
    public float GunDamagePoint
    {
        set { gunDamagePoint = value; }
    }
    public int Direction
    {
        set { direction = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (gunDamagePoint <= 0.1)
        {
            Destroy(gameObject);
        }

        StartCoroutine(DestroyObject());
        rigidbody2D = GetComponent<Rigidbody2D>();
        Move();
    }


    void Move()
    {
        rigidbody2D.velocity = new Vector2(direction * projectileSpeed * 100 * Time.deltaTime, rigidbody2D.velocity.y);
    }

    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageHandler collisionDamageHandler = collision.gameObject.GetComponent<DamageHandler>();
        collisionDamageHandler.GetDamaged(gunDamagePoint);
        Destroy(gameObject);
    }


}
