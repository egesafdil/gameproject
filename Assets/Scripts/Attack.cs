﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    //Configs
    [SerializeField] float gunDamagePoint = 10f;

    //Caches
    [SerializeField] Projectile projectile;
    [SerializeField] Transform projectilePosition;

    private Animator animator;

    void Start()
    {
        if(projectile == null)
        {
            return;
        }

        animator = GetComponent<Animator>();
    }

    void Update()
    {
        
        Shoot();
    }

    void Shoot()
    {
        if(Input.GetMouseButtonDown(0) && (projectile != null || projectilePosition != null))
        {
            animator.SetTrigger("Attack");
            StartCoroutine(InstantiateProjectile());
        }
    }

    IEnumerator InstantiateProjectile()
    {
        yield return new WaitForEndOfFrame();
        int direction = 1;

        if(gameObject.transform.position.x > projectilePosition.position.x)
        {
            direction = -1;
        }

        Projectile newProjectile = Instantiate(projectile, projectilePosition.position, Quaternion.identity);
        newProjectile.GunDamagePoint = gunDamagePoint;
        newProjectile.Direction = direction;
    }
}
