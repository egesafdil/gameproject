﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region SerializeFields
    [SerializeField] float walkingSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    [SerializeField] float jetpackTimer = 5f;
    [SerializeField] float jetpackVerticalForce = 5f;
    [SerializeField] float jetpackMaxVerticalSpeed = 2f;
    [SerializeField] float jetpackHorizontalForce = 5f;
    [SerializeField] float jetpackMaxHoritantalSpeed = 2f;
    #endregion

    #region Caches
    Rigidbody2D playerRigidBody;
    Animator animator;
    BoxCollider2D feet;
    private float maxJetpackTimer = 5f;
    #endregion

    #region Conditions
    bool isGrounded = false;
    bool isWalking = false;
    bool isJetting = false;
    #endregion

    public float JetpackTimer
    {
        get { return jetpackTimer; }
    }

    void Start()
    {
        playerRigidBody = GetComponent<Rigidbody2D>();
        feet = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
        maxJetpackTimer = jetpackTimer;
    }

    void Update()
    {
        IsGrounded();
        Walk();
        Flip();
        Jump();
        JetPack();
    }

    void IsGrounded()
    {
        if (feet.IsTouchingLayers(LayerMask.GetMask("Foreground")))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

    void Walk()
    {
        if (isJetting || !isGrounded)
        {
            return;
        }

        float movementFlow = Input.GetAxis("Horizontal");
        Vector2 velocity = new Vector2(movementFlow * walkingSpeed * 100 * Time.deltaTime, playerRigidBody.velocity.y);
        playerRigidBody.velocity = velocity;

        isWalking = Mathf.Abs(playerRigidBody.velocity.x) > Mathf.Epsilon;
        animator.SetBool("isWalking", isWalking);
    }

    void Flip()
    {
        if (isWalking || isJetting)
        {
            transform.localScale = new Vector2(Mathf.Sign(playerRigidBody.velocity.x), 1f);
        }
    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Vector2 jumpVelocity = new Vector2(0f, jumpSpeed);
            playerRigidBody.velocity += jumpVelocity;
        }
    }

    void JetPack()
    {
        float movementFlowVertical = Input.GetAxis("Vertical");
        float movementFlowHorizontal = Input.GetAxis("Horizontal");

        if(jetpackTimer > 0f && (movementFlowVertical > Mathf.Epsilon || (!isGrounded && Mathf.Abs(movementFlowHorizontal) > Mathf.Epsilon)))
        {
            animator.SetBool("isWalking", false);
            isJetting = true;
            float movementHorizontal = movementFlowHorizontal * jetpackHorizontalForce * 5 * Time.deltaTime;
            float movementVertical = movementFlowVertical * jetpackVerticalForce * 5 * Time.deltaTime;
            
            if(Mathf.Abs(movementHorizontal) >= jetpackMaxHoritantalSpeed)
            {
                movementHorizontal = Mathf.Sign(movementFlowHorizontal) * jetpackMaxHoritantalSpeed;
            }
            if(movementVertical >= jetpackMaxVerticalSpeed)
            {
                movementVertical = jetpackMaxVerticalSpeed;
            }

            playerRigidBody.AddForce(new Vector2(movementHorizontal, movementVertical), ForceMode2D.Impulse);
            animator.SetBool("isJetting", isJetting);
            jetpackTimer -= Time.deltaTime;
        }
        else
        {
            isJetting = false;
            animator.SetBool("isJetting", isJetting);
            if(jetpackTimer < maxJetpackTimer && isGrounded)
            {
                StartCoroutine(FillJetpack());
            }
        }
    }

    IEnumerator FillJetpack()
    {
        jetpackTimer += Time.deltaTime * 2;
       
        yield return null;
    }

    

}

