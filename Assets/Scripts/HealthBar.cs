﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image healthBar;
    PlayerHealth player;

    private void Start()
    {
        healthBar = GetComponent<Image>();
        player = FindObjectOfType<PlayerHealth>();

    }
    private void Update()
    {
        float currentHealth = player.Health;
        healthBar.fillAmount = currentHealth / 100f;
    }

}
