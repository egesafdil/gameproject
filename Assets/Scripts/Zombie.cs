﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    #region Configs
    [SerializeField] float walkingSpeed = 5f;
    [SerializeField] float attackRange = 5f;
    #endregion

    #region Caches
    Rigidbody2D rigidbody2D;
    Animator animator;
    Player player;
    #endregion

    #region AI
    bool isBusy = false;
    bool isWalking = false;
    #endregion 

    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isBusy)
        {
            IdleWalk();
        }
        Flip();
        GoToTarget();
    }
    void Flip()
    {
        if (isWalking)
        {
            transform.localScale = new Vector2(Mathf.Sign(rigidbody2D.velocity.x), 1f);
        }
    }

    void IdleWalk()
    {

    }

    void Walk(float walking)
    {
        if (Mathf.Abs(walking) > 0f)
        {
            Vector2 velocity = new Vector2(walking * walkingSpeed * 100 * Time.deltaTime, rigidbody2D.velocity.y);
            rigidbody2D.velocity = velocity;

            isWalking = Mathf.Abs(rigidbody2D.velocity.x) > Mathf.Epsilon;
            animator.SetBool("isWalking", isWalking);
        }
        else
        {
            isWalking = false;
            animator.SetBool("isWalking", isWalking);
        }
    }

    void GoToTarget()
    {
        Vector2 zombiePosition = gameObject.transform.position;
        Vector2 playerPosition = player.transform.position;

        float distanceBetweenTarget = Mathf.Abs(zombiePosition.x - playerPosition.x);
        Debug.Log("Distance" + distanceBetweenTarget);
        if ((distanceBetweenTarget < attackRange) && distanceBetweenTarget > 2f)
        {
            Walk(1 * Mathf.Sign(playerPosition.x - zombiePosition.x));
        }
        else if(distanceBetweenTarget <= 2f)
        {
            rigidbody2D.velocity = new Vector2(0f, rigidbody2D.velocity.y);
            Walk(0f);
        }
        else
        {
            Walk(0f);
        }
    }

}
