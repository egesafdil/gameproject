﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageHandler : MonoBehaviour
{
    [SerializeField] float health = 100f;

    private PlayerHealth playerHealth;
    bool isPlayer = false;

    void Start()
    {
        if (GetComponent<PlayerHealth>())
        {
            isPlayer = true;
            playerHealth = GetComponent<PlayerHealth>();
        }
    }

    void Update()
    {
        if (isPlayer) { playerHealth.Health = health; }
    }

    public void GetDamaged(float damagePoint)
    {
        health -= damagePoint;
    }

}
