﻿using UnityEngine;
using UnityEngine.UI;

public class JetBar : MonoBehaviour
{
    private Image jetBar;
    Player player;
    private float maxFuel = 5f;

    private void Start()
    {
        jetBar = GetComponent<Image>();
        player = FindObjectOfType<Player>();
        maxFuel = player.JetpackTimer;
    }
    private void Update()
    {
        float currentFuel = player.JetpackTimer;
        jetBar.fillAmount = currentFuel / maxFuel;
    }
}
