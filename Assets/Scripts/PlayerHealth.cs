﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    private float health = 100f;

    public float Health
    {
        get { return health; }
        set { health = value; }
    }

    
}
